<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'/libraries/REST_Controller.php');	

class Test extends REST_Controller{

	public function __construct()
	{
		parent::__construct('rest');
		$this->load->model('test_model');
	}
	
	public function goodsbyid_get(){
		$id = $this->get('id');
		$result = $this->test_model->getGoodsById($id);
		if($result){
			$this->response(array('result'=>$result, 'status'=>200),200);
		}else{
			$this->response(array('message'=>'无相关商品','status'=>404),200);
		}

	}
}