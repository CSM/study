<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test_model extends CI_Model{

    public function __construct()
    {
        $this->load->database();
        $this->tb_goods = 'goods';
    }

    public function getGoodsById($id, $fields = "*"){
        $this->db->select($fields);
        $query = $this->db->get_where($this->tb_goods, "goods_id =" . $id);
        $result = $query->result_array();
        return $result;
    }
}